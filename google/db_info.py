import mysql.connector
def connectionBoranet():
    conn = mysql.connector.connect(
            host = 'erp.boranet.net', 
            user = 'boradmin', 
            password = 'Bora1234!@' ,
            db = 'boranet',
            auth_plugin='mysql_native_password')
 
    c = conn.cursor()
    return c , conn

# url navigation을 위한 정보들 
def getTotalClientInfo_DB():

    mycursor,mydb = connectionBoranet()
    mycursor.execute(  "select * \
                        from(       \
                            SELECT DISTINCT b.bunho, b.name2, b.city, b.zipcode, b.tel\
                            FROM bora_customer b, bora_customer_snsInfo a\
                            WHERE a.cust_no = b.bunho AND b.act_gb='1'\
                        ) AS i\
                        ORDER BY i.bunho ASC")

    res = mycursor.fetchall()

    ret = [] # tuple to list

    for I in map(lambda x:list(x),res):
        if " & " in I[1]:
            I[1] = I[1].replace("&", "  ")# google.com/search?q="~~~~~" 에 &가 들어가면 & 뒤에검색이 무시됨,  # & to empty space
        if "&" in I[1]:
            I[1] = I[1].replace("&", " ")

        if I[0] == 1057:                #커플스 검색이 안되서 키워드 변경
            I[1] = "couples 미국&"
        if I[1] == 'ENJOY NAIL    SPA':
            I[3] = ""
        for i in range(len(I)):
            if I[i] == None:
                I[i] =""
        ret.append(I)

    return ret

#데이터베이스 속 회사의 댓글이 몇개인지 회사별로 데이터를 가져옴
def reviewTotalDictionary():
    dictionary = {}
    mycursor,mydb = connectionBoranet()
    mycursor.execute(  "select  a.customer_id,count(a.customer_id)\
                        from bora_custom_review_gmb a \
                        group by a.customer_id")
                        # ORDER BY i.bunho ASC")
    
    for item in mycursor.fetchall():
        dictionary[item[0]] = item[1]
    return dictionary

#customer_id 가 id인 최근 3명의 댓글을 가져옴 
def reviewrecentWriter(id):
    mycursor,mydb = connectionBoranet()
    mycursor.execute(  "SELECT a.writer,a.content           \
                        FROM boranet.bora_custom_review_gmb a\
                        WHERE a.customer_id = "+str(id)+"           \
                        ORDER BY a.bunho DESC                \
                        LIMIT 3;")
    name = []
    content = []
    for i in mycursor.fetchall():
        name.append(i[0])
        content.append(i[1])

    return name,content
def InsertGoogleReview(customer_id,rate,content,write_dt,timestamp):
    mycursor,mydb = connectionBoranet()

    sql = "INSERT IGNORE INTO bora_custom_review_gmb (bunho, customer_id,rate,content,write_dt,reg_dt) VALUES (%s, %s, %s, %s, %s, %s)"
    val = (None,str(customer_id), float(rate), content, write_dt,timestamp)
    mycursor.execute(sql, val)

    mydb.commit()

def InsertGoogleReviewList(customer_id,rate,content,write_dt,timestamp,writer):
    mycursor,mydb = connectionBoranet()

    val = []

    sql = "INSERT IGNORE INTO bora_custom_review_gmb (bunho, customer_id,rate,content,write_dt,reg_dt, writer) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    for i in range(len(customer_id)-1,-1,-1):
         val.append((None, str(customer_id[i]), float(rate[i]), content[i], write_dt[i], timestamp[i], writer[i]))

    affected_count = mycursor.executemany(sql,val)

    print(len(customer_id)," UPDATED :",affected_count)
    mydb.commit()
