from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import random
import time
from bs4 import BeautifulSoup
import db_info  as db_comments
from datetime import datetime


def ChromeOptions(ip):
    chrome_options = Options()
    chrome_options.add_argument("--lang=en-US")
    # chrome_options.add_argument(f'--proxy-server={ip}')
    user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
    
    chrome_options = Options()
    chrome_options.add_argument("--lang=en-US")
    # chrome_options.add_argument("start-maximized")
    chrome_options.add_argument("user-agent="+user_agent)
    chrome_options.add_argument("--headless")  
    chrome_options.add_experimental_option("prefs", {"profile.managed_default_content_settings.images": 2})

    return chrome_options  


def driverwait(driver):
    driver.implicitly_wait(random.randrange(500,1500,1))

def getOriginalCommentFromAutoTranslate(txt):
    
    if "(Translated by Google)" in txt or "(original)" in txt:
        if txt.startswith("(Translated by Google)"):
            return txt.split("(Original)")[1]
        elif txt.startswith("(Original)"):
            return txt.split("(Translated by Google)")[0].split("(Original)")[1]
        elif "(Translated by Google)" in txt:
            return txt.split("(Translated by Google)")[0]
    
    return txt

def NoneChecking(element):
    if element:
        element = element.text
    else:
        element = ""
    return element

"""
Description : 크롤링 후, read more 버튼 클릭 후, 파싱 시작합니다

Args:
    page_source     : 페이지 가져오기 위한 argument
    clientInfo      : DB에서 가져온 고객 정보 [bunho, name2, city, zipcode, tel]
    totalReviewInfo : 데이터 베이스 안에 있는 회사별 댓글수{회사bunho1:댓글 수, 회사bunho2: 댓글 수 ...}를 가져옵니다
"""
def htmlParsing(page_source, clientInfo, totalReviewInfo):
    nameList = []
    customer_idList = []
    timestampList = []
    starList = []
    reviewList = []  
    write_at_list = []
    html = page_source
    soup=BeautifulSoup(html,'html.parser')
    comments=soup.find_all("div", {"class": "WMbnJf"})
    for comment in comments:

        name = comment.find("div",{"class": "TSUbDb"})
        star = comment.find("span",{"class": "fTKmHE99XE4__star fTKmHE99XE4__star-s"})["aria-label"]
        review = comment.find("div",{"class": "Jtu6Td"})
        
        if review.find("span",{"class": "review-full-text"}):
            review = review.find("span",{"class": "review-full-text"})
        write_dt = comment.find("span",{"class": "dehysf"})
        
        name = NoneChecking(name)
        review  = NoneChecking(review)
        write_dt = NoneChecking(write_dt)

        write_at_list.append(write_dt)
        review = getOriginalCommentFromAutoTranslate(review)
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        nameList.append(name)
        starList.append(star[6:9])
        reviewList.append(review)
        customer_idList.append(clientInfo[0])
        timestampList.append(timestamp)
    
    # 구글은 스크롤링을 해서 모든 댓글을 한번에 가져 오기때문에, 가장 최근에 올린 댓글의 작성자과 content를 가져와서 크롤링 데이터와 비교후, 선택적으로 업데이트
    # 제한 : user이나 content를 변경하거나, 최근 댓글 3개가 짧은 시간안에 삭제 될 시, 중단점을 찾지 못하고 최근 20개의 댓글만 크롤링함 
    # 해결 : 해당 회사 댓글을 디비에서 모두 삭제 후 다시 크롤링
    writerList,contentList = db_comments.reviewrecentWriter(clientInfo[0])
    if clientInfo[0] in totalReviewInfo.keys(): #한 번은 크롤링 된 적이 있는 회사
        for i in range(len(nameList)):
            if nameList[i] in writerList :       #wrtierList(가장 최근에 올린 게시자 3명)이 댓글에 있는지 
                nameList = nameList[0:i]
                starList = starList[0:i]
                reviewList = reviewList[0:i]
                customer_idList =customer_idList[0:i]
                timestampList =timestampList[0:i]
                break
    
    #Insert filtered data
    db_comments.InsertGoogleReviewList(customer_idList, starList,  reviewList, write_at_list, timestampList,nameList)

    
    # result = pd.DataFrame({'Name':nameList,'Star':starList,'Review':reviewList,'CustomerID':customer_idList,'timestamp':timestampList}) #csv저장위한 코드
    # return result


"""
Description : 구글 검색에서 구글 리뷰에 들어가서 최신순으로 정렬하는 것까지
"""
def NavigateToReview(driver): 
    if driver.execute_script("return document.getElementsByClassName('hqzQac').length") != 0: # IF "Google reviews" 클릭 할수 있는 element가 존재하면
        time.sleep(random.random()+1)
        driver.find_element_by_css_selector("span.hqzQac").click() #Click "Google reviews"
        driverwait(driver)
        time.sleep(random.random()+1)
        driver.find_element_by_css_selector("span.BpGBNe").click() #Click "Sort by"
        driverwait(driver)
        time.sleep(random.random()+1)
        driver.find_elements_by_css_selector("div.znKVS")[1].click() #Click "Newest"
        driverwait(driver)
        time.sleep(random.random()+1)

        return True
    else:
        return False

def gotoUrl(driver,url):
    driver.get(url)
    driverwait(driver)

    #정보 페이지로 이동으로 인해 -> More about ~~~ click
    if driver.execute_script("return document.getElementsByClassName('hide-focus-ring iXqz2e aI3msd pSO8Ic vk_arc').length") ==1:
        driver.execute_script("return document.getElementsByClassName('hide-focus-ring iXqz2e aI3msd pSO8Ic vk_arc')[0].click()")


"""
Description : 크롤링 후 read more버튼을 하나하나 클릭해 줍니다.
"""
def clickMoreButtons(driver):
    loading = 0
 
    while loading < 3:
        time.sleep(1)
        if driver.execute_script("return document.getElementsByClassName('review-more-link').length") == 0:
            break
        moreButtons = driver.find_elements_by_class_name("review-more-link")
        if moreButtons:
            for moreButton in moreButtons:
                moreButton.click()
            time.sleep(2)
            break
        loading +=1

"""
Description : 스크롤링이 계속 합니다.

Args:
    driver          : chomre driver
    clientInfo      : DB에서 가져온 고객 정보 [bunho, name2, city, zipcode, tel]
    totalReviewInfo : 데이터 베이스 안에 있는 회사별 댓글수{회사bunho1:댓글 수, 회사bunho2: 댓글 수 ...}를 가져옵니다

"""
def scrolling_scraping(driver,clientInfo ,totalReviewInfo):
    lastHeight = driver.execute_script("return document.getElementsByClassName('review-dialog-list')[0].scrollHeight")
    trial =0


    # 디비에 리뷰가 1개 이상인 회사: 데이터베이스에 댓글이 있으면 2번만 스크롤링하고 파싱, 
    # 전제조건 : 해당 회사의 댓글이 일부분만 있으면 안됌(댓글이 하나만 있어도, 딱 두번만 크롤링 하고 종료)
    if clientInfo[0] in totalReviewInfo.keys(): 
        driver.execute_script("document.getElementsByClassName('review-dialog-list')[0].scrollTo(0,document.getElementsByClassName('review-dialog-list')[0].scrollHeight);")
        time.sleep(1) 
        clickMoreButtons(driver)                                                        #Click "more" buttons before parsing data
        time.sleep(random.random()+1)
        htmlParsing(driver.page_source, clientInfo, totalReviewInfo)
    # 새로운 회사 - 무한 스크롤링 후 크롤링
    # trial : ajax 요청이 늦게 오면, 스크롤링이 끝난지 알아 scraping을 하게 되는데, 최대 5번 동안 기다립니다.
    else:                                       
        while True:
            driver.execute_script("document.getElementsByClassName('review-dialog-list')[0].scrollTo(0,document.getElementsByClassName('review-dialog-list')[0].scrollHeight);")
            time.sleep(1) 
        
             
            newHeight = driver.execute_script("return document.getElementsByClassName('review-dialog-list')[0].scrollHeight")
            if newHeight == lastHeight and trial < 5:
                trial +=1
                continue
            
            #크롤링이 다 끝난 후
            if newHeight == lastHeight and trial >= 5:
                clickMoreButtons(driver)                                                            #Click "more" buttons before parsing data
                time.sleep(random.random()+1)
                htmlParsing(driver.page_source, clientInfo, totalReviewInfo)               #Parsing data
                # result.to_csv('./output/'+"20200109"+'.csv',index=True, mode='a')                 #Crawled data to csv
                break
            else:
                trial =0
                lastHeight = newHeight



"""
Description : 데이터베이스에 있는 특정 회사의 댓글과 chrome에서 검색한 회사의 댓글 수를 비교

Args:
    driver          : chomre driver
    clientInfo      : DB에서 가져온 고객 정보 [bunho, name2, city, zipcode, tel]
    totalReviewInfo : 데이터 베이스 안에 있는 회사별 댓글수{회사bunho1:댓글 수, 회사bunho2: 댓글 수 ...}를 가져옵니다

Returns:
    True: 데이터베이스 댓글 수!= 구글에서 보여지는 댓글수(크롤링 대상)
    False: 데이터베이스 댓글 수 == 구글에서 보여지는 댓글 수
"""
def reviewUpdated(driver, clientInfo, totalReviewInfo):

    if driver.execute_script("return document.getElementsByClassName('hqzQac').length") != 0:   # IF 리뷰가 크롬 검색시 존재하면        
        if clientInfo[0] in totalReviewInfo.keys():                                         # IF 회사 데이터 베이스에 등록이 되어 있으면
            #예를 들어 1,247 google reivews를 int(1247)로 바꾸고 데이터베이스의 값과 비교 
            if int(driver.execute_script("return document.getElementsByClassName('hqzQac')[0].innerText.split(' Google')[0]").replace(",","")) ==totalReviewInfo[clientInfo[0]]:
                return False
    return True



""" 
Description : 크롤링 대상인지 아닌지 구글서칭에 따라 다른 값을 리턴하는 함수
 3가지 케이스로 나눠짐
  - 업데이트 할 것이 없음
  - 없데이트 할 것을 찾음
  - 사이트를 찾을 수 없음

Returns:
    "Nothing Updated": 업데이트 할 댓글이 없음
    "Wrong URL": google reviews가 없는 url로 라우팅 됨
"""
def waitUntilNavigationCompleted(driver,clientInfo, totalReviewInfo):
    if not reviewUpdated(driver, clientInfo, totalReviewInfo):  #review가 updated 됐으면 true ,안됐으면 False,
        return "Nothing Updated"

    #AJAX요청이 늦어져서 가끔 늦게 로딩되거나 x,xxx google reviews를 못찾는 경우가 있는데 이 경우 3번 시도
    succ = NavigateToReview(driver)
    NavigateTry = 0
    while not succ:
        time.sleep(1)
        succ = NavigateToReview(driver)
        print( "Cannot find \"",clientInfo ,"\" site's reivew")
        NavigateTry +=1
        if NavigateTry ==3: # Wrong input
            return "Wrong URL"

        if succ: #Code for late loading
            break
"""
def google_multiprocessing_Exmaple( ithChrome,, totalReviewInfo ):
    ip = ithChrome[1]
    clientName = ithChrome[0]
    #READ "INPUT"
    # clientNameList = googleLib.ReadList()
    #CHROME DRIVER SETTING
    driver = webdriver.Chrome('chromedriver',options=ChromeOptions(ip))

    # for clientName in clientNameList:
    gotoUrl(driver,"https://www.google.com/search?q="+clientName)
    waitUntilNavigationCompleted(driver,clientName,, totalReviewInfo)
    scrolling_scraping(driver,clientName,totalReviewInfo)
    driver.quit()
"""
