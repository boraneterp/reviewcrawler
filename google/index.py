import googleLibrary as googleLib
import db_info as dbInfo
# import proxy_scrap as proxy_scrap
from selenium import webdriver
# from multiprocessing import Pool
import random
import time
import os

if __name__ == "__main__":
    #MULTIPROCESSING WITH IP_LIST - open ip는 쉽게 block당하고, vpn으로는 데이터 베이스에 접근을 할수 없어서 사용하지 않았습니다.
    # pool = Pool(processes=1)
    # clientName = googleLib.ReadList()
    # ip_list = proxy_scrap.ipList()

    # iterList = IPandClientNameMapping(clientName, ip_list)
    # print(iterList)
    # pool.map(google_multiprocessing_Exmaple,iterList.items())

    #READ "INPUT"
    # clientNameList = googleLib.ReadList()

    ChromedriverAbsPath = os.path.join(os.path.dirname( os.path.abspath(__file__)),"../",'chromedriver_linux79')

    clientInfoList = dbInfo.getTotalClientInfo_DB()    #데이터베이스에서 custemer_id,address,number 등 구글링을 통해 찾을 수 있게 구성합니다.
    totalReviewInfo = dbInfo.reviewTotalDictionary()   #데이터 베이스 안에 있는 회사별 댓글수{회사1:댓글 수, 회사2: 댓글 수 ...}를 가져옵니다

    #CHROME DRIVER SETTING
    driver = webdriver.Chrome(executable_path=ChromedriverAbsPath,options=googleLib.ChromeOptions("ip"))

    for clientInfo in clientInfoList:
        url ="https://www.google.com/search?q="+clientInfo[1]+", "+clientInfo[2]+", "+clientInfo[3]+" "+clientInfo[4]
        print("----------------------------------")
        print("searching url : ", url)
        print("client_id : ", clientInfo[0])
        googleLib.gotoUrl(driver,url)                   #url  Navigation

        #DB에 있는 개수와 url을 통해 들어갔을 때 크롤링 대상의 댓글 개수 현황을 보여줍니다.
        if driver.execute_script("return document.getElementsByClassName('hqzQac').length"):
            print("google   review count : " + driver.execute_script("return document.getElementsByClassName('hqzQac')[0].innerText"))
        else:
            print("google cannot find")
        if clientInfo[0] in totalReviewInfo.keys():
            print("Database review count : " , totalReviewInfo[clientInfo[0]])
        else:
            print("Database review count : " , 0)
    
        res = googleLib.waitUntilNavigationCompleted(driver,clientInfo,totalReviewInfo)
        
        if res == "Wrong URL":
            continue
        elif res =="Nothing Updated":
            time.sleep(random.random()*3+0.5)
            continue
        else:
            googleLib.scrolling_scraping(driver,clientInfo, totalReviewInfo)

    driver.quit()   
