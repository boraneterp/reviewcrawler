import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import random
import time
from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def ChromeOptions():
    user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
    prefs = {"profile.managed_default_content_settings.images": 2}
    
    chrome_options = Options()
    chrome_options.add_argument("--lang=en-US")
    chrome_options.add_argument(f'user-agent={user_agent}')
    

    chrome_options.add_experimental_option("prefs", prefs)
    return chrome_options 


def SCRAPING_page(driver):

    
    html = driver.page_source
    soup=BeautifulSoup(html,'html.parser')

    odd = soup.select("tr.odd")
    even = soup.select("tr.even")

    res = []
    for element in odd:
        tdList = element.select("td")
        if tdList[2].text =="US" and tdList[4].text =="anonymous" and tdList[6].text =="yes":
            res.append(tdList[0].text+":"+tdList[1].text)

    for element in even:
        tdList = element.select("td")
        if tdList[2].text =="US" and tdList[4].text =="anonymous" and tdList[6].text =="yes":
            res.append(tdList[0].text+":"+tdList[1].text)
    return res
    

def ipList():
    driver = webdriver.Chrome('chromedriver',options=ChromeOptions())
    driver.maximize_window()

    driver.get("https://www.us-proxy.org/")
    driver.execute_script("return document.getElementsByClassName('ui-state-default sorting')[4].click()")
    time.sleep(0.654)
    driver.execute_script("return document.getElementsByClassName('ui-state-default sorting')[5].click()")
    time.sleep(0.865)
    driver.execute_script("return document.getElementsByClassName('ui-state-default sorting_asc')[0].click()")
    res = SCRAPING_page(driver)

    driver.quit()
    return res


def proxy_test_multiprocessing( ithChrome ):

    ua_list = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0',
               'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0']
    user_agent = random.choice(ua_list)   
    # user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
    
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument(f'--proxy-server={ithChrome[1]}')
    chrome_options.add_argument("start-maximized")
    chrome_options.add_argument("--lang=en-US")
    chrome_options.add_experimental_option("prefs", {"profile.managed_default_content_settings.images": 2})
    chrome_options.add_argument(f'user-agent={user_agent}')
    chrome = webdriver.Chrome(options=chrome_options)

    chrome.get("https://www.google.com/search?q=myipaddress")



def IPandClientNameMapping(clientName, ip_list):
    CN_IP_dict = {}

    for i in range(0,len(clientName)):
        CN_IP_dict[clientName[i]] = ip_list[i %len(ip_list)]

    return CN_IP_dict