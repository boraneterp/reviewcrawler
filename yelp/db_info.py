import mysql.connector
from collections import Counter 
from collections import OrderedDict 

#TOTAL DB 고객 정보
def getTotalClientInfo_DB():

    mycursor,mydb = connectionBoranet()
    mycursor.execute(  "select a.cust_no, b.name2, a.yelp_bookmark_title                    \
                        from bora_customer b, bora_customer_snsInfo a                       \
                        where a.cust_no = b.bunho AND b.act_gb='1' AND (a.sns_name='Yelp')")

    res = OrderedDict() 
    for i in mycursor.fetchall():
        if i[2] != "null":
            res[i[2].lower()]=i[0] # i[0] is cost_no

    return res

"""
Description : 해당 customer_id값에 해당하는 댓글 수를 리턴하는 함수
                distinct를 한 이유는 한 명이 2개의 댓글을 작성시 1개의 댓글로 인식
Args:
    bunho : customer_id 값

Returns:
    res[0] : 해당 customer_id값에 해당하는 댓글 수
"""
def commentsCount(mycursor,bunho):
    
    mycursor.execute(  "select count(distinct(a.writer))       \
                        from bora_custom_review_yelp a          \
                        where a.customer_id = "+str(bunho))

    res = mycursor.fetchone()
    return res[0]


def connectionBoranet():
    conn = mysql.connector.connect(
            host = 'erp.boranet.net', 
            user = 'boradmin', 
            password = 'Bora1234!@' ,
            db = 'boranet',
            auth_plugin='mysql_native_password')
 
    c = conn.cursor()
    return c , conn


"""
Description : 작성자 수를 unique하게 한 후(set 사용) 해당 언어 별 작성자 수(Counter 사용)를 리턴
Args:
    bunho : customer_id 값

Returns:
    Counter(list) - ex) {'English': 192, 'French': 1, 'Spanish': 1}
"""
def reviewCountForOneLanguage(bunho):
    lis = []
    mycursor,mydb = connectionBoranet()
    mycursor.execute(  "select  a.writer, a.language\
                        from bora_custom_review_yelp a \
                        WHERE a.customer_id = "+str(bunho))    

    for writer,language in set(mycursor.fetchall()):
        lis.append(language)

    return Counter(lis)


def totalReviewCount(bunho):
    mycursor,mydb = connectionBoranet()
    mycursor.execute(  "select count(*)\
                        from bora_custom_review_yelp a \
                        WHERE a.customer_id = "+str(bunho))

    return mycursor.fetchone()[0]


"""
Description : 데이터 베이스에 새로운 댓글을 insert 하는 함수

Args:
    customer_id : 회사 넘버
    rate : 평점
    content : 내용
    write_dt : 작성일
    timestamp : 업로드 날
    writer : 작성자
    language : 작성된 언어
"""
def InsertYelpReviewList(customer_id,rate,content,write_dt,timestamp,writer,language):
    mycursor,mydb = connectionBoranet()

    val = []
    sql = "INSERT IGNORE INTO bora_custom_review_yelp (bunho, customer_id,rate,content,write_dt,reg_dt, writer,language) VALUES (%s, %s, %s, %s, %s, %s, %s,%s)"
    
    for i in range(len(customer_id)-1,-1,-1):
         val.append((None, str(customer_id[i]), float(rate[i]), content[i], write_dt[i], timestamp[i], writer[i],language[i]))

    mycursor.executemany(sql,val)

    mydb.commit()


"""
Description : 최근 3명의 댓글 작성자를 호출

Args:
    bunho : customer_id 값
    language : 작성된 언어

Returns:
    lst : 3명 작성자의 이름을 리턴 
"""
def RecentThreeCommentsWriter(bunho,language):
    lst = []
    sql = "select  a.writer from bora_custom_review_yelp a WHERE a.customer_id = "+str(bunho)+" AND a.language='"+str(language)+"' order by write_dt desc limit 3"
    mycursor,mydb = connectionBoranet()
    mycursor.execute(sql)
    
    for item in mycursor.fetchall():
        lst.append(item[0])
    return lst

