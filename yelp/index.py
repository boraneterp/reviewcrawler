import yelpLibrary as yelpLib
from selenium import webdriver
import db_info as dbInfo
import time
import random
import os
"""
collectorDICT,candidatess의 return exmaple =  {
    'hudson park dental': {'rate': 5.0, 'count': 1, 'url': 'https://www.yelp.com//biz/hudson-park-dental-kearny'}, 
    'metro spa': {'rate': None, 'count': None, 'url': 'https://www.yelp.com//biz/metro-spa-west-columbia-2'}, 
    'enjoy nail & spa': {'rate': 5.0, 'count': 1, 'url': 'https://www.yelp.com//biz/enjoy-nail-and-spa-bethlehem'},
}
"""

ChromedriverAbsPath = os.path.join(os.path.dirname( os.path.abspath(__file__)),"../","chromedriver_linux79") #linux
# ChromedriverAbsPath = os.path.join(os.path.dirname( os.path.abspath(__file__)),"../","chromedriver_window") #window


collectorDICT = yelpLib.yelpCollectionCralwer(ChromedriverAbsPath)
candidates = yelpLib.updateCandidatesDict(collectorDICT)


# candidates = {'soju haus': {'url': 'https://www.yelp.com//biz/soju-haus-new-york', 'bunho': 27}}
# print("the number of candidates are", len(candidates.keys()))
# print(candidates.values())
# print("----------------------------------------------")



driver = webdriver.Chrome(ChromedriverAbsPath,options=yelpLib.ChromeOptions())
driver.maximize_window()



for k,dbObj in candidates.items():
    yelpLib.gotoUrl(driver,dbObj["url"])                                #URL로 Navigation
    yelpLib.waitAJAXrequest(driver)
    lang_count_dict = yelpLib.MAKE_Language_countDict(driver)           #{'English': '1394', 'German': '17', 'Chinese': '3'}와 같이 크롤링 대상의 댓글 개수 확인
    yelpLib.ITERATE_LangWithSort(driver, lang_count_dict,dbObj)
    time.sleep(random.random()*10)
driver.close()

