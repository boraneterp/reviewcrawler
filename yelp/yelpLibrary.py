# import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import random
import time
from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import db_info as dbInfo
from datetime import datetime
from collections import OrderedDict

def ChromeOptions():
    user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
    prefs = {"profile.managed_default_content_settings.images": 2}
    # prefs = {"profile.managed_default_content_settings.images": 2,"profile.default_content_settings.cookies": 2}
    
    chrome_options = Options()
    chrome_options.add_argument("--lang=en-US")
    chrome_options.add_argument("user-agent="+user_agent)
    
    chrome_options.add_argument("--headless")   #on for linux
    # chrome_options.add_argument("--headless")  off for window
    
    # chrome_options.add_experimental_option("Set-Cookie","HttpOnly;Secure;SameSite=Strict")
    # chrome_options.addArguments("user-data-dir=/path/to/your/custom/profile");


    chrome_options.add_experimental_option("prefs", prefs)
    return chrome_options 


"""
Description : yelp my list 크롤러 
"""
def yelpCollectionCralwer(ChromedriverAbsPath):
    driver = webdriver.Chrome(ChromedriverAbsPath,options=ChromeOptions())
    # driver.maximize_window()
    
    gotoUrl(driver,"https://www.yelp.com/collection/wlmXmJNrN3FXLs7RiQ4vfA")
    collectorDICT = CollectorScrolling(driver) #옐프 북마크 정보
    driver.close()

    return collectorDICT
    


"""
Description : yelp my list 크롤링을 위해 무한 스크롤링

Args:
    driver : chrome driver
"""
def CollectorScrolling(driver):
    lastHeight = driver.execute_script("return document.getElementsByClassName('collection-content collection-left-pane')[0].scrollHeight")
    trial =0
                                    # New Company~~~~~~~~ Need to scrape all reviews
    while True:       
        driver.execute_script("document.getElementsByClassName('collection-content collection-left-pane')[0].scrollTo(0,document.getElementsByClassName('collection-content collection-left-pane')[0].scrollHeight);")
        time.sleep(1) 
    
        #prevent stop scrolling from slow Ajax request 
        newHeight = driver.execute_script("return document.getElementsByClassName('collection-content collection-left-pane')[0].scrollHeight")
        if newHeight == lastHeight and trial < 5:
            trial +=1
            continue
        
        #After scrolling,
        if newHeight == lastHeight and trial >= 5:
            return collectorCrawling(driver)            
        else:
            trial =0
            lastHeight = newHeight


"""
Description : mylist 무한 스크롤링 후 navigating 할 url과 댓글 수를 dictaionry로 저장

Args:
    driver : chrome driver

Returns:
    collectorDICT : mylist의 값들을 크롤링 한 dictionary
"""
def collectorCrawling(driver):
    html = driver.page_source
    soup=BeautifulSoup(html,'html.parser')
    collection_items = soup.find_all("li", {"class":"collection-item"})

    collectorDICT = OrderedDict() 
    for item in collection_items:
        name,rate,count ="",None,None
        name = item.find("div",{"class":"media-title"}).text.replace("\n","").lower()
        url = item.find("a",{"class":"biz-name"})["href"]
        if item.find("div",{"class":"biz-rating"}):
            rate = float(item.find("div",{"class":"i-stars"})["title"][0:3])
            count = int(item.find("span",{"itemprop":"reviewCount"}).text)
        collectorDICT[name] ={"rate":rate, "count":count,"url":"https://www.yelp.com/"+url}

    return collectorDICT

"""
Description : yelp my list 크롤링 후,데이터베이스와 댓글 수를 비교 후 크롤링 대상을 걸러 내는 함수

Args:
    collectorDICT : yelp에서 mylist 크롤링 한 결과

Returns:
    candidates : 크롤링 할 대상
"""
def updateCandidatesDict(collectorDICT):
    dbDICT = dbInfo.getTotalClientInfo_DB()    #TOTAL DB 고객 정보 # 크롤링 대상아님
    mycursor,mydb = dbInfo.connectionBoranet() #DB 연결
    candidates = OrderedDict() 

    for index,value in collectorDICT.items():

        db_count = 0
        if value['rate'] is not None:   #북마크에 크롤링할 댓글이 있는 경우

            db_count = dbInfo.commentsCount(mycursor,dbDICT[index])
            if db_count == value["count"]: # db에서 댓글 개수 카운트 한것 == 북마크에서 댓글 총개수
                continue #This site is not the candidate for renewing db

            else:
                candidates[index] = {"url":value["url"],"bunho":dbDICT[index]}
    print("The number of Customer in DB : ",len(dbDICT))
    print("Final Candidates for crawling: ",len(candidates))

    return candidates
def driverwait(driver):
    driver.implicitly_wait(random.randrange(500,1500,1))

def gotoUrl(driver,url):
    driver.get(url)
    driverwait(driver)


"""
Description : 해당 회사에 댓글이 어느 언어로 몇개의 댓글이 있는지 알아내는 함수

Args:
    driver : chomre driver

Returns:
    lang_total_dict: 언어-댓글 수 dictionary 
    ex){'English': '1394', 'German': '17', 'Chinese': '3', 'Japanese': '3', 'French': '2', 'Italian': '2', 'Portuguese': '1', 'Spanish': '1'}
"""
def MAKE_Language_countDict(driver):
    
    sortbyORlanguage = driver.execute_script("return document.getElementsByClassName('lemon--span__373c0__3997G text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-weight--bold__373c0__3HYJa')[1].innerText")
    
    lang_total_dict = OrderedDict()

    
    if sortbyORlanguage.startswith("English"): 
        
        #OPEN LANGUAGE TAP
        driver.execute_script("return document.getElementsByClassName('lemon--span__373c0__3997G text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-weight--bold__373c0__3HYJa')[1].click()")
        
        #MAKE {LANGUAGE : N.comment}
        languageList = driver.execute_script("return document.getElementsByClassName('lemon--li__373c0__1r9wz option__373c0__3dKAe')")
        for i in range(len(languageList)):
            separating =  driver.execute_script("return document.getElementsByClassName('lemon--li__373c0__1r9wz option__373c0__3dKAe')["+str(i)+"].innerText").split(" (")
            lang_total_dict[separating[0]] = int(separating[1][0:-1])
        
        #CLOSE LANGUAGE TAP
        driver.execute_script("return document.getElementsByClassName('lemon--span__373c0__3997G text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-weight--bold__373c0__3HYJa')[1].click()")
        
        
        return lang_total_dict

    else: 
        #only one langauge
        #EXPECTED OUTPUT = False
        return False

def waitAJAXrequest(driver):
    time.sleep(1)
    waitForElementPresent(driver, "div.lemon--div__373c0__1mboc.spinner-container__373c0__N6Hff.border-color--default__373c0__YEvMS")
            
"""
Description : 해당 회사에 댓글이 어느 언어로 댓글이 몇 개 있는지 알아내는 함수

Args:
    driver : chomre driver
    lang_count_dict : 크롤링할 데이터의 언어 별 개수 ex){'English': '1394', 'German': '17', 'Chinese': '3'}
    dbCompnayInfo : 데이터베이스에서 크롤링 할 회사의 정보  ex){'url': 'https://www.yelp.com//biz/soju-haus-new-york', 'bunho': 27}

Returns:
    lang_total_dict: 언어-댓글 수 dictionary 
    ex){'English': '1394', 'German': '17', 'Chinese': '3', 'Japanese': '3', 'French': '2', 'Italian': '2', 'Portuguese': '1', 'Spanish': '1'}
"""
def ITERATE_LangWithSort(driver, lang_count_dict,dbCompnayInfo):
    
    DBreviewCountByLang = dbInfo.reviewCountForOneLanguage(dbCompnayInfo['bunho'])#db에서 언어별 개추 추출 
    totalReview = int(driver.execute_script("return document.getElementsByClassName('lemon--p__373c0__3Qnnj text__373c0__2pB8f text-color--mid__373c0__3G312 text-align--left__373c0__2pnx_ text-size--large__373c0__1568g')[0].innerText").split(" ")[0])
    print("=========================================================")
    print(dbCompnayInfo['url'])
    if lang_count_dict:
        print("Reviews - counted by language in web: ",lang_count_dict)
    else:
        print("Reviews - counted by language in web: English:",totalReview)
    print("Reviews - counted by langauge in DB : ",DBreviewCountByLang)
    
    #MORE THAN 2 LANGUAGE
    if lang_count_dict:
        for i, (key,value) in enumerate(lang_count_dict.items()):

            #DB에 이 언저자체가 존재하는지 
            if key in list(DBreviewCountByLang.keys()):
                #IF 언어별로 추출한 댓글 개수가  web상의 개수랑 같으면 continue
                if DBreviewCountByLang[key] == value:
                    continue
            #LANGUAGE OPEN
            driver.execute_script("return document.getElementsByClassName('lemon--span__373c0__3997G text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-weight--bold__373c0__3HYJa')[1].click()")
            driver.implicitly_wait(random.randrange(500,1500,1))
            time.sleep(0.5+ random.random())

            #LANGUAGE SELECT
            driver.execute_script("return document.getElementsByClassName('lemon--div__373c0__1mboc u-padding-t-half u-padding-r2 u-padding-b-half u-padding-l2 border-color--default__373c0__2oFDT overflow--hidden__373c0__8Jq2I nowrap__373c0__1_N1j')["+str(i)+"].click()") # Language setting
            driver.implicitly_wait(random.randrange(500,1500,1))
            time.sleep(0.5+ random.random())

            #SORT BY OPEN
            driver.execute_script("return document.getElementsByClassName('lemon--span__373c0__3997G text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-weight--bold__373c0__3HYJa')[0].click()")
            driver.implicitly_wait(random.randrange(500,1500,1))
            time.sleep(0.5+ random.random())

            #SORT BY SELECT
            driver.execute_script("return document.getElementsByClassName('lemon--div__373c0__1mboc u-padding-t-half u-padding-r2 u-padding-b-half u-padding-l2 border-color--default__373c0__2oFDT overflow--hidden__373c0__8Jq2I nowrap__373c0__1_N1j')[1].click()")
            driver.implicitly_wait(random.randrange(1499,3001,1))
            time.sleep(0.5+ random.random())
            

            waitAJAXrequest(driver)
            #DB에 이 언어가 존재하는면 
            if key in list(DBreviewCountByLang.keys()):
                ITERATE_pages(dbCompnayInfo,key,value,driver,DBreviewCountByLang[key])
            else:
                ITERATE_pages(dbCompnayInfo,key,value,driver,0)
            waitAJAXrequest(driver)

    #ONLY ONE LANGUAGE
    else:
            totalReview = int(driver.execute_script("return document.getElementsByClassName('lemon--p__373c0__3Qnnj text__373c0__2pB8f text-color--mid__373c0__3G312 text-align--left__373c0__2pnx_ text-size--large__373c0__1568g')[0].innerText").split(" ")[0])
            
            #SORT BY OPEN
            driver.execute_script("return document.getElementsByClassName('lemon--span__373c0__3997G text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-weight--bold__373c0__3HYJa')[0].click()")
            driver.implicitly_wait(random.randrange(500,1500,1))
            
            #SORT BY SELECT
            driver.execute_script("return document.getElementsByClassName('lemon--div__373c0__1mboc u-padding-t-half u-padding-r2 u-padding-b-half u-padding-l2 border-color--default__373c0__2oFDT overflow--hidden__373c0__8Jq2I nowrap__373c0__1_N1j')[1].click()")
                                                                           
            driver.implicitly_wait(random.randrange(1499,3001,1))


            time.sleep(random.random()+3) #로딩 기다리는 시간
            
            if "English" in DBreviewCountByLang.keys():
                ITERATE_pages(dbCompnayInfo,"English",totalReview,driver,DBreviewCountByLang["English"])
            else:
                ITERATE_pages(dbCompnayInfo,"English",totalReview,driver,0)
    

"""
Description : 해당 언어로 페이지를 iterate하는 함수

Args:
    dbCompnayInfo   : 데이터베이스에서 크롤링 할 회사의 정보  ex){'url': 'https://www.yelp.com//biz/soju-haus-new-york', 'bunho': 27}
    key             : 무슨 언어로 된 댓글들을 크롤링할 지
    value           : 해당언어는 chrome상에 몇개로 표시되어 있는가
    driver          : chrome driver
    visited         : 데이터베이스에 key(언어)로 된 댓글 개수가 총 몇 개인지  , 0보다 크면 방문한적이 있음
"""
def ITERATE_pages(dbCompnayInfo,key,value,driver,visited):

    #value가 20보다 크다는 것은 크롤링 할 페이지가 2개 이상이라는 것
    if value >20:
        while True:
            
            SCRAPING_page(driver,dbCompnayInfo,key) #현재 페이지 크롤링
            if visited > 0:   #IF db에 댓글 개수가 0 개가 아니면(it means 처음으로 들어온 사이트가 아니므로 맨 처음 페이지만 크롤링)
                break

            #IF CURRENT PAGE IS LAST PAGE
            if 0 == driver.execute_script("return document.getElementsByClassName('lemon--div__373c0__1mboc navigation-button-text__373c0__38ysY u-space-l2 border-color--default__373c0__2oFDT').length") :
                break
            else:
                #Click next button
                driver.execute_script("return document.getElementsByClassName('lemon--div__373c0__1mboc navigation-button-text__373c0__38ysY u-space-l2 border-color--default__373c0__2oFDT')[0].click()")
            waitAJAXrequest(driver)
            
            
            time.sleep(random.random()+3) #로딩 기다리는 시간

    #value가 20보다 작다는 것은 크롤링 할 페이지가 1개 라는 것        
    else:
        #scraping
        SCRAPING_page(driver,dbCompnayInfo,key)

"""
Description : readmore 버튼을 클릭해서 full text가 보여지게 함

Args:
    driver          : chrome driver
"""
def CLICK_readmore(driver):

    readmoreIdx = []
    readmoreButtons = driver.execute_script("return document.getElementsByClassName('lemon--p__373c0__3Qnnj text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-size--small__373c0__3SGMi')")
    
    for i in range(0,len(readmoreButtons)):
        if "photo" in readmoreButtons[i].text or "photos" in readmoreButtons[i].text :
            pass
        elif readmoreButtons[i].text =="Read more":
            readmoreIdx.append(i)

    for i in range(len(readmoreIdx)-1,-1,-1):
        driver.execute_script("return document.getElementsByClassName('lemon--p__373c0__3Qnnj text__373c0__2pB8f text-color--inherit__373c0__w_15m text-align--left__373c0__2pnx_ text-size--small__373c0__3SGMi')["+str(readmoreIdx[i])+"].click()")

    time.sleep(random.random()+2) # 페이지 로딩 
def NoneOrText(input):
    if input == None:
        return None
    else:
        return input.text
def NoneOrAriaLabel(input):
    if input == None:
        return None
    else:
        return input["aria-label"]

def waitForElementPresent(driver, selector):
    # return WebDriverWait(driver, 60).until(EC.presence_of_element_located(selector))
    return WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))


"""
Description : 최근 댓글 작성자 3명을 불러와 업데이트 할지 말지를 결정 후 크롤링

Args:
    driver          : chrome driver
    dbCompnayInfo   : 데이터베이스에서 크롤링 할 회사의 정보  ex){'url': 'https://www.yelp.com//biz/soju-haus-new-york', 'bunho': 27}
    key             : 사용 언어 이름
"""
def SCRAPING_page(driver,dbCompnayInfo,key):

    #Bring Recent 3 users
    userList = dbInfo.RecentThreeCommentsWriter(dbCompnayInfo['bunho'],key)


    html = driver.page_source
    soup=BeautifulSoup(html,'html.parser')


    #CLICK "READMORE"
    CLICK_readmore(driver)

    comments = soup.select("li.lemon--li__373c0__1r9wz.u-space-b3.u-padding-b3")

    Lcustomer_id,Lrate,Lcontent,Lwrite_dt,Ltimestamp,Lwriter,Llanguage = [],[],[],[],[],[],[]

    #PARSING
    for comment in comments:
        userNames = comment.select("span.lemon--span__373c0__3997G.text__373c0__2pB8f.fs-block.text-color--inherit__373c0__w_15m.text-align--left__373c0__2pnx_.text-weight--bold__373c0__3HYJa")
        uploadDates = comment.select("span.lemon--span__373c0__3997G.text__373c0__2pB8f.text-color--mid__373c0__3G312.text-align--left__373c0__2pnx_")    
        rates = comment.select("div.lemon--div__373c0__1mboc.i-stars__373c0__Y2F3O.border-color--default__373c0__YEvMS.overflow--hidden__373c0__3Usf-")       
        reviews = comment.select("p.lemon--p__373c0__3Qnnj.text__373c0__2pB8f.comment__373c0__3EKjH.text-color--normal__373c0__K_MKN.text-align--left__373c0__2pnx_")
                                     

        #한사람이 여러개의 댓글을 쓸 경우, 여러개의 댓글이 한 번에 보여지기때문에 case를 나눴습니다.
        if len(reviews) >1:
            for i in range(len(reviews)):
                userName = NoneOrText(userNames[0])
                uploadDate = NoneOrText(uploadDates[i*2])
                rate = NoneOrAriaLabel(rates[i])[0]
                review = NoneOrText(reviews[i])

                # print(userName,"\t\t",uploadDate,"\t\t",rate)
                if userName in userList : #이름이 최근 댓글 올린 사람중에 있고 & 언어가 같은지 
                    break
                Lcustomer_id.append(dbCompnayInfo['bunho'])
                Lrate.append(rate)
                Lcontent.append(review)
                Lwrite_dt.append(changeFormOf_write_at(uploadDate))
                Ltimestamp.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                Lwriter.append(userName)
                Llanguage.append(key)


        else:
            userName = NoneOrText(userNames[0])
            uploadDate = NoneOrText(uploadDates[0])
            rate = NoneOrAriaLabel(rates[0])[0]
            review = NoneOrText(reviews[0])

            if userName in userList: #중복체크
                break
            Lcustomer_id.append(dbCompnayInfo['bunho'])
            Lrate.append(rate)
            Lcontent.append(review)
            Lwrite_dt.append(changeFormOf_write_at(uploadDate))
            Ltimestamp.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            Lwriter.append(userName)
            Llanguage.append(key)


    print("customer_id : ",dbCompnayInfo['bunho']," & saving : ",len(Lcustomer_id),", items in the DB" )
    #DB save
    dbInfo.InsertYelpReviewList(Lcustomer_id,Lrate,Lcontent,Lwrite_dt,Ltimestamp,Lwriter,Llanguage)


"""
Description : 최근 사용자 3명을 불러올때 정렬을 write_at으로 할건데, type이 varchar이므로 이 함수를 사용해서 desc순으로 정렬할수있게 foramt 변환

Args:
    uploadDate : ex) 3/25/2013

Return: 
    string     : ex) 2013-03-25
"""
def changeFormOf_write_at(uploadDate):
    
    
    try:
        res = uploadDate.split("/")
        month =res[0]
        day = res[1]
        if len(month) ==1:
            month = "0" + month
        if len(day) ==1:
            day = "0" + day

        return res[2]+"-"+month+"-"+day
    except:
        return "0000-00-00"
